﻿using UnityEngine;
using System.Collections;

public class FadeCamera : MonoBehaviour {
	
	public float distance = -1;
	
	public Material fadeMaterial = null;
	public static bool IsFading { get; private set; }
	public GameObject spawn;

	public static YieldInstruction FadeCurrent(MonoBehaviour runner, Material material, Color color, float alphaEnd, float duration)
	{
		if (material.color.a == alphaEnd)
			return null;

		return runner.StartCoroutine(FadeEnum(material, color, material.color.a, alphaEnd, duration));
	}

	public static YieldInstruction Fade(MonoBehaviour runner, Material material, Color color, float alphaStart, float alphaEnd, float duration)
	{
		return runner.StartCoroutine(FadeEnum(material, color, alphaStart, alphaEnd, duration));
	}

	static IEnumerator FadeEnum(Material material, Color color, float alphaStart, float alphaEnd, float duration)
	{
		IsFading = true;

		float alpha = alphaStart;

		color.a = alphaStart;
		material.color = color;

		float speed = (alphaEnd - alphaStart) / duration;
		float t = 0;

		while (t < duration)
		{
			yield return new WaitForEndOfFrame();
			t		+= Time.deltaTime;
			alpha	+= Time.deltaTime * speed;
			color.a = alpha;
			material.color = color;
		}

		color.a = alphaEnd;
		material.color = color;

		IsFading = (alphaEnd != 0f);
	}

	/// <summary>
	/// Renders the fade overlay when attached to a camera object
	/// </summary>
	void OnPostRender()
	{

		if (IsFading) {
			spawn.SetActive(false);
			Debug.Log ("la");
			fadeMaterial.SetPass (0);
			GL.PushMatrix ();
			GL.LoadOrtho ();
			GL.Color (fadeMaterial.color);
			GL.Begin (GL.QUADS);
			GL.Vertex3 (0f, 0f, distance);
			GL.Vertex3 (0f, 1f, distance);
			GL.Vertex3 (1f, 1f, distance);
			GL.Vertex3 (1f, 0f, distance);
			GL.End ();
			GL.PopMatrix ();
		} else {
			spawn.SetActive(true);
		}
	}

	 void Start(){
		StartCoroutine(FadeEnum(fadeMaterial, Color.black, 1, 0, 4));

	}
}